# Adding HTML list support for Android TextView

This is a supporting sample project for my answer <http://stackoverflow.com/a/17365740/262462>.
Initial TagHandler code is taken from my commits at <https://bitbucket.org/bitbeaker-dev-team/bitbeaker> after tag `release-v2.4.1`.

If you find the code in this repo useful, please vote my answer at StackOverflow up. Improvements via pull requests are greatly appreciated.

## License

Copyright 2013-2015 Juha Kuitunen

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

<http://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
